from bitbot.models import Trade, TradeProfit

symbols = ['btcusd','ethusd','ethbtc','adabtc','bchbtc','eosbtc','ltcbtc','trxbtc','xrpbtc']
lot_size = {'btcusd': 6000, 'ethusd': 6000, 'ethbtc': 30, 'adabtc':90000, 'bchbtc':15, 'eosbtc':1500, 'ltcbtc':150, 'trxbtc':350000, 'xrpbtc':25000}


def run():
    for strategy in ['mamacd', 'ma200', 'ma20']:
        for symbol in symbols:
            trades = Trade.objects.filter(strategy=strategy, symbol=symbol, exchange='bitmex')
            for i, trade in enumerate(trades):
                if trade.side == 'Close':
                    prev_trade = trades[i-1]
                    if prev_trade.side == 'Buy':
                        if 'usd' in symbol:
                            pos = lot_size[symbol]/float(prev_trade.price)
                            profit= (float(trade.price) - float(prev_trade.price))*pos
                        else:
                            profit = lot_size[symbol]*float(trade.price) - lot_size[symbol]*float(prev_trade.price)
                    elif prev_trade.side == 'Sell':
                        if 'usd' in symbol:
                            pos = lot_size[symbol]/float(prev_trade.price)
                            profit = (float(prev_trade.price) - float(trade.price))*pos
                        else:
                            profit = lot_size[symbol]*float(prev_trade.price) - lot_size[symbol]*float(trade.price)
                    try:
                        tp = TradeProfit.objects.get(entry__id=prev_trade.id)
                        tp.exit = trade
                        tp.pnl = profit
                        tp.status = 'closed'
                        tp.save()
                    except:
                        print(trade.id, prev_trade.id)


                else:
                    tp = TradeProfit(entry=trade, status='open')
                    tp.save()