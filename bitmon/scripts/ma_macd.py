from __future__ import absolute_import

import os
import time
import talib
import numpy
import ccxt
import sys

from market_maker.market_maker import OrderManager
from market_maker.market_maker import ExchangeInterface
from time import sleep
import sys
from datetime import datetime
from os.path import getmtime
import random
import requests
import atexit
import signal

from market_maker import bitmex
from market_maker.settings import settings
from market_maker.utils import log, constants, errors, math
from bitbot.models import Trade
from tastypie.utils.timezone import now

# Used for reloading the bot - saves modified times of key files
lot_size = {'btcusd': 2000, 'ethusd': 2000}


#
# Helpers
#
logger = log.setup_custom_logger('root')
bitmex_ccxt = ccxt.bitmex({"urls":{"api":'https://testnet.bitmex.com'}})

# params:
symbol = 'BTC/USD'
timeframe = '5m'
limit = 750
params = {'partial': False}  # ←--------  no reversal
symbols = {
    'btcusd': 'BTC/USD',
    'ethusd': 'ETH/USD'}

class CustomExchangeInterface(ExchangeInterface):
    def __init__(self, dry_run=False, symbol='XBTUSD'):

        self.dry_run = dry_run
        self.symbol = symbol
        self.bitmex = bitmex.BitMEX(base_url="https://testnet.bitmex.com/api/v1/", symbol=self.symbol,
                                    apiKey=settings.TEST_API_KEY, apiSecret=settings.TEST_API_SECRET,
                                    orderIDPrefix=settings.ORDERID_PREFIX, postOnly=settings.POST_ONLY,
                                    timeout=settings.TIMEOUT)


class CustomOrderManager(OrderManager):
    def __init__(self):

        self.exchange = CustomExchangeInterface(settings.DRY_RUN)
        # Once exchange is created, register exit handler that will always cancel orders

        atexit.register(self.exit)
        signal.signal(signal.SIGTERM, self.exit)

        logger.info("Using symbol %s." % self.exchange.symbol)

        if settings.DRY_RUN:
            logger.info("Initializing dry run. Orders printed below represent what would be posted to BitMEX.")
        else:
            logger.info("Order Manager initializing, connecting to BitMEX. Live run: executing real trades.")

        self.start_time = datetime.now()
        self.instrument = self.exchange.get_instrument()
        self.starting_qty = self.exchange.get_delta()
        self.running_qty = self.starting_qty
        self.reset()

    """A sample order manager for implementing your own custom strategy"""
    def parse_ticker(self, symbol):
        if symbol == 'BTC/USD':
            ticker = self.exchange.get_ticker('XBTUSD')
        elif symbol == 'ETH/USD':
            ticker = self.exchange.get_ticker('ETHUSD')                
        else:
            ticker = self.exchange.get_ticker(symbol)
        sell_ticker = ticker["sell"]
        buy_ticker =  ticker["buy"]
        return buy_ticker, sell_ticker

    def place_orders(self):
        # implement your custom strategy here
    # pay attention to since with respect to limit if you're doing it in a loop
        for key,symbol in symbols.items():
            since = bitmex_ccxt.milliseconds() - limit  *60* 1000 

            candles = bitmex_ccxt.fetch_ohlcv(symbol , timeframe, since, limit, params)
            num_candles = len(candles)
            if symbol == 'BTC/USD':
                self.exchange = CustomExchangeInterface(settings.DRY_RUN,'XBTUSD')
                symbol = 'XBTUSD'
            elif symbol == 'ETH/USD':
                self.exchange = CustomExchangeInterface(settings.DRY_RUN,'ETHUSD')
                symbol ='ETHUSD'
            else:      
                self.exchange = CustomExchangeInterface(settings.DRY_RUN, symbol)
            buy_orders = [] 
            sell_orders = []

            ar_data =  numpy.column_stack(candles)
            # open_candle = ar_data[1].astype(float)
            # high = ar_data[2].astype(float)
            # low = ar_data[3].astype(float)
            close = ar_data[4].astype(float)

            ma_20 = talib.MA(close, timeperiod=20, matype=0)
            ma_50 = talib.MA(close, timeperiod=50, matype=0)
            count_ma20 = len(ma_20)
            count_ma50 = len(ma_50)

            macd, macdsignal, macdhist = talib.MACD(close, fastperiod=12, slowperiod=26, signalperiod=9)
            count_macdhist = len(macdhist)
            try:
                lt = Trade.objects.filter(strategy="mamacd", symbol=key, exchange='bitmex').last()
                last_trade = lt.side
            except:
                last_trade = "Close"
            buy_ticker, sell_ticker = self.parse_ticker(symbol)
            if last_trade != 'Close' and lt:
                if last_trade == 'Buy':
                    if close[num_candles-1] < ma_20[count_ma20-1] or macdhist[count_macdhist-1] <0:
                        
                        self.exchange.cancel_all_orders()

                       
                        self.running_qty = self.exchange.get_delta(symbol)

                        while self.running_qty != 0:
                            self.exchange.cancel_all_orders()
                            buy_orders = []
                            sell_orders = []

                            if self.running_qty > 0:
                                sell_orders.append({'execInst': 'ParticipateDoNotInitiate', 'price': sell_ticker, 'orderQty': abs(self.running_qty),   'side': "Sell"})
                            elif self.running_qty < 0:
                                buy_orders.append({'execInst': 'ParticipateDoNotInitiate', 'price': buy_ticker, 'orderQty':  abs(self.running_qty), 'side': "Buy"})
                            print("\nclose all orders\n")

                            self.converge_orders(buy_orders, sell_orders)
                            time.sleep(5)
                            self.exchange.cancel_all_orders()
                            self.running_qty = self.exchange.get_delta(symbol)
                            buy_ticker, sell_ticker = self.parse_ticker(symbol)

                        trade = Trade(timestamp=now(),
                                      order_value=lot_size[key],
                                      price=sell_ticker,
                                      side="Close",
                                      strategy="mamacd",
                                      exchange="bitmex",
                                      symbol=key)
                        trade.save()


                elif last_trade == 'Sell':
                    if close[num_candles-1] > ma_20[count_ma20-1] or macdhist[count_macdhist-1] >0:

                        self.exchange.cancel_all_orders()

                       
                        self.running_qty = self.exchange.get_delta(symbol)

                        while self.running_qty != 0:
                            self.exchange.cancel_all_orders()
                            buy_orders = []
                            sell_orders = []

                            if self.running_qty > 0:
                                sell_orders.append({'execInst': 'ParticipateDoNotInitiate', 'price': sell_ticker, 'orderQty': abs(self.running_qty),   'side': "Sell"})
                            elif self.running_qty < 0:
                                buy_orders.append({'execInst': 'ParticipateDoNotInitiate', 'price': buy_ticker, 'orderQty':  abs(self.running_qty), 'side': "Buy"})
                            print("\nclose all orders\n")

                            self.converge_orders(buy_orders, sell_orders)
                            time.sleep(5)
                            self.exchange.cancel_all_orders()
                            self.running_qty = self.exchange.get_delta(symbol)
                            buy_ticker, sell_ticker = self.parse_ticker(symbol)
                        trade = Trade(timestamp=now(),
                                      order_value=lot_size[key],
                                      price=buy_ticker,
                                      side="Close",
                                      strategy="mamacd",
                                      exchange="bitmex",
                                      symbol=key)
                        trade.save()


            elif ma_20[count_ma20-1] > ma_50[count_ma50-1]:
                if macdhist[count_macdhist -1] >0:
                    if last_trade == 'Close':
                        
                        self.exchange.cancel_all_orders()
                        self.running_qty = self.exchange.get_delta(symbol)

                        while self.running_qty < lot_size[key]:
                            self.exchange.cancel_all_orders()
                            buy_ticker, sell_ticker = self.parse_ticker(symbol)
                            buy_orders = []
                            sell_orders = []
                            if self.running_qty < lot_size[key]:
                                buy_orders.append({'execInst': 'ParticipateDoNotInitiate', 'price': buy_ticker, 'orderQty': lot_size[key] -self.running_qty, 'side': "Buy"})
                                print("\nBuy {}\n".format(lot_size[key]-self.running_qty))

                            elif self.running_qty == 0:
                                buy_orders.append({'execInst': 'ParticipateDoNotInitiate', 'price': buy_ticker, 'orderQty': lot_size[key], 'side': "Buy"})
                                print("\nBuy {}\n".format(lot_size[key]))

                            print("\nBuy-EdgeCase:- {} \n".format(self.running_qty))
                            self.converge_orders(buy_orders, sell_orders)
                            time.sleep(5)
                            self.exchange.cancel_all_orders()
                            self.running_qty = self.exchange.get_delta(symbol)
                            print('buy')

                        trade = Trade(timestamp=now(),
                                      order_value=lot_size[key],
                                      price=buy_ticker,
                                      side="Buy",
                                      strategy="mamacd",
                                      exchange="bitmex",
                                      symbol=key)
                        trade.save()


                    elif last_trade == 'Sell':
                        
                        self.exchange.cancel_all_orders()

                        
                        self.running_qty = self.exchange.get_delta(symbol)

                        while self.running_qty < lot_size[key]:
                            self.exchange.cancel_all_orders()
                            buy_ticker, sell_ticker = self.parse_ticker(symbol)
                            buy_orders = []
                            sell_orders = []
                            if self.running_qty < lot_size[key]:
                                buy_orders.append({'execInst': 'ParticipateDoNotInitiate', 'price': buy_ticker, 'orderQty': 2*lot_size[key] -self.running_qty, 'side': "Buy"})
                                print("\nBuy {}\n".format(lot_size[key]-self.running_qty))

                            elif self.running_qty == 0:
                                buy_orders.append({'execInst': 'ParticipateDoNotInitiate', 'price': buy_ticker, 'orderQty': 2*lot_size[key], 'side': "Buy"})
                                print("\nBuy {}\n".format(lot_size[key]))

                            print("\nBuy-EdgeCase:- {} \n".format(self.running_qty))
                            self.converge_orders(buy_orders, sell_orders)
                            time.sleep(5)
                            self.exchange.cancel_all_orders()
                            self.running_qty = self.exchange.get_delta(symbol)
                            print('buy')

                        trade = Trade(timestamp=now(),
                                      order_value=2*lot_size[key],
                                      price=buy_ticker,
                                      side="Buy",
                                      strategy="mamacd",
                                      exchange="bitmex",
                                      symbol=key)
                        trade.save()

            elif ma_20[count_ma20-1] < ma_50[count_ma50-1]:
                if macdhist[count_macdhist -1] < 0:
                    if last_trade == 'Close':
                        
                        self.exchange.cancel_all_orders()


                        self.running_qty = self.exchange.get_delta(symbol)

                        while abs(self.running_qty) < lot_size[key]:
                            self.exchange.cancel_all_orders()
                            buy_ticker, sell_ticker = self.parse_ticker(symbol)
                            buy_orders = []
                            sell_orders = []
                            if self.running_qty > -lot_size[key]:

                                sell_orders.append({'execInst': 'ParticipateDoNotInitiate', 'price': sell_ticker, 'orderQty': lot_size[key] + self.running_qty, 'side': "Sell"})
                                print("Sell {}\n".format(lot_size[key]+self.running_qty))

                            elif self.running_qty == 0:

                                sell_orders.append({'execInst': 'ParticipateDoNotInitiate', 'price': sell_ticker, 'orderQty': lot_size[key], 'side': "Sell"})
                                print("Sell {}\n".format(lot_size[key]))

                            print("\nSell-EdgeCase:- {} \n".format(self.running_qty))
                            self.converge_orders(buy_orders, sell_orders)

                            time.sleep(5)
                            self.exchange.cancel_all_orders()
                            self.running_qty = self.exchange.get_delta(symbol)

                        trade = Trade(timestamp=now(),
                                      order_value=lot_size[key],
                                      price=sell_ticker,
                                      side="Sell",
                                      strategy="mamacd",
                                      exchange="bitmex",
                                      symbol=key)
                        trade.save()

                    elif last_trade == 'Buy':
                        
                        self.exchange.cancel_all_orders()

                     
                        self.running_qty = self.exchange.get_delta(symbol)

                        while abs(self.running_qty) < lot_size[key]:
                            buy_ticker, sell_ticker = self.parse_ticker(symbol)
                            self.exchange.cancel_all_orders()
                            buy_orders = []
                            sell_orders = []
                            if self.running_qty > -lot_size[key]:

                                sell_orders.append({'execInst': 'ParticipateDoNotInitiate', 'price': sell_ticker, 'orderQty': 2*lot_size[key] + self.running_qty, 'side': "Sell"})
                                print("Sell {}\n".format(lot_size[key]+self.running_qty))

                            elif self.running_qty == 0:

                                sell_orders.append({'execInst': 'ParticipateDoNotInitiate', 'price': sell_ticker, 'orderQty': 2*lot_size[key], 'side': "Sell"})
                                print("Sell {}\n".format(lot_size[key]))

                            print("\nSell-EdgeCase:- {} \n".format(self.running_qty))
                            self.converge_orders(buy_orders, sell_orders)

                            time.sleep(5)
                            self.exchange.cancel_all_orders()
                            self.running_qty = self.exchange.get_delta(symbol)

                        trade = Trade(timestamp=now(),
                                      order_value=2*lot_size[key],
                                      price=sell_ticker,
                                      side="Sell",
                                      strategy="mamacd",
                                      exchange="bitmex",
                                      symbol=key)
                        trade.save()
            print(ma_20[count_ma20-1], ma_50[count_ma50-1], macdhist[count_macdhist-1], key)

    def run_loop(self):
        while True:
            sys.stdout.write("-----\n")
            sys.stdout.flush()

            self.check_file_change()
            sleep(300)

            # This will restart on very short downtime, but if it's longer,
            # the MM will crash entirely as it is unable to connect to the WS on boot.
            if not self.check_connection():
                logger.error("Realtime data connection unexpectedly closed, restarting.")
                self.restart()

            self.sanity_check()  # Ensures health of mm - several cut-out points here
            self.print_status()  # Print skew, delta, etc
            self.place_orders()

    def run():
        order_manager = CustomOrderManager()

        # Try/except just keeps ctrl-c from printing an ugly stacktrace
        try:
            order_manager.run_loop()
        except (KeyboardInterrupt, SystemExit):
            sys.exit()



def run():
    while True:
        try:

            CustomOrderManager.run()
        except:
            pass
        else:
            break
