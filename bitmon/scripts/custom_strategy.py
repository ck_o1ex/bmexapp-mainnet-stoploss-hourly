from __future__ import absolute_import

import os
import time
import talib
import numpy
import ccxt
import sys

from market_maker.market_maker import OrderManager, ExchangeInterface
from time import sleep
import sys
from datetime import datetime
from os.path import getmtime
import random
import requests
import atexit
import signal

from market_maker import bitmex
from market_maker.settings import settings
from market_maker.utils import log, constants, errors, math
from bitbot.models import Trade
from tastypie.utils.timezone import now

# Used for reloading the bot - saves modified times of key files


#
# Helpers
#
logger = log.setup_custom_logger('root')
bitmex_ccxt = ccxt.bitmex({"urls":{"api":'https://testnet.bitmex.com'}})

# params:
symbol = 'BTC/USD'
timeframe = '5m'
limit = 750
params = {'partial': False}  # ←--------  no reversal
thresholds = [10]

from time import sleep
import sys
from datetime import datetime
from os.path import getmtime
import random
import requests
import atexit
import signal


# Used for reloading the bot - saves modified times of key files
import os
watched_files_mtimes = [(f, getmtime(f)) for f in settings.WATCHED_FILES]


#
# Helpers
#
logger = log.setup_custom_logger('root')


class CustomExchangeInterface(ExchangeInterface):
    def __init__(self, dry_run=False):

        self.dry_run = dry_run
        if len(sys.argv) > 1:
            self.symbol = 'XBTUSD'
        else:
            self.symbol = settings.SYMBOL
        self.bitmex = bitmex.BitMEX(base_url="https://testnet.bitmex.com/api/v1/", symbol=self.symbol,
                                    apiKey=settings.TEST_API_KEY, apiSecret=settings.TEST_API_SECRET,
                                    orderIDPrefix=settings.ORDERID_PREFIX, postOnly=settings.POST_ONLY,
                                    timeout=settings.TIMEOUT)




class CustomOrderManager(OrderManager):
    def __init__(self):

        self.cci_20 = []
        self.cci_40 = []
        self.tot_mins = 0

        self.exchange = CustomExchangeInterface(settings.DRY_RUN)
        # Once exchange is created, register exit handler that will always cancel orders

        atexit.register(self.exit)
        signal.signal(signal.SIGTERM, self.exit)

        logger.info("Using symbol %s." % self.exchange.symbol)

        if settings.DRY_RUN:
            logger.info("Initializing dry run. Orders printed below represent what would be posted to BitMEX.")
        else:
            logger.info("Order Manager initializing, connecting to BitMEX. Live run: executing real trades.")

        self.start_time = datetime.now()
        self.instrument = self.exchange.get_instrument()
        self.starting_qty = self.exchange.get_delta()
        self.running_qty = self.starting_qty
        self.reset()
  

    """A sample order manager for implementing your own custom strategy"""

    def place_orders(self):
        # implement your custom strategy here
    # pay attention to since with respect to limit if you're doing it in a loop
        buy_orders = [] 
        sell_orders = []
        since = bitmex_ccxt.milliseconds() - limit * 60 * 1000

        candles = bitmex_ccxt.fetch_ohlcv(symbol , timeframe, since, limit, params)
        num_candles = len(candles)
        # self.cci_20, self.cci_40 = calculate_cci(candles)
        if self.tot_mins == 0 or (self.tot_mins % 20 == 15 and self.tot_mins % 40 == 35):
            ar_data =  numpy.column_stack(candles)
            open_candle = ar_data[1].astype(float)
            high = ar_data[2].astype(float)
            low = ar_data[3].astype(float)
            close = ar_data[4].astype(float)
            self.cci_20 = talib.CCI(high, low, close, 20)
            self.cci_40 = talib.CCI(high, low, close, 40)
        elif self.tot_mins % 20 == 15 and self.tot_mins % 40 != 35:
            ar_data =  numpy.column_stack(candles)
            open_candle = ar_data[1].astype(float)
            high = ar_data[2].astype(float)
            low = ar_data[3].astype(float)
            close = ar_data[4].astype(float)
            self.cci_20 = talib.CCI(high, low, close, 20)
        elif self.tot_mins % 40 == 35 and self.tot_mins % 20 != 15:
            ar_data =  numpy.column_stack(candles)
            open_candle = ar_data[1].astype(float)
            high = ar_data[2].astype(float)
            low = ar_data[3].astype(float)
            close = ar_data[4].astype(float)
            self.cci_40 = talib.CCI(high, low, close, 40)
        print('\n{}: O: {} H: {} L:{} C:{} cci20:{} cci40:{}\n'.format(
            bitmex_ccxt.iso8601(candles[num_candles - 1][0]),
            candles[num_candles - 1][1],
            candles[num_candles - 1][2],
            candles[num_candles - 1][3],
            candles[num_candles - 1][4],
        self.cci_20[num_candles -1], self.cci_40[num_candles-1]))
        print(len(self.cci_20))

        margin = self.exchange.get_margin()
        position = self.exchange.get_position()
        self.running_qty = self.exchange.get_delta()
        ticker = self.exchange.get_ticker()
        buy_ticker = ticker["sell"]
        sell_ticker =  ticker["buy"]
        for threshold in thresholds:
            lt = Trade.objects.filter(threshold=threshold).last()
            last_trade = lt.side if lt else "Close"

            if self.cci_20[num_candles -1]>threshold and self.cci_40[num_candles-1]>threshold:
                if last_trade == 'Close':
                    trade = Trade(threshold=threshold,
                        cci_20=self.cci_20[num_candles -1],
                                  cci_40=self.cci_40[num_candles-1],
                                  timestamp=now(),
                                  order_value=settings.LOT_SIZE,
                                  price=buy_ticker,
                                  side="Buy")
                    trade.save()
                    self.exchange.cancel_all_orders()

                    buy_orders.append({'price': buy_ticker, 'orderQty':settings.LOT_SIZE , 'side': "Buy" })
                elif last_trade == 'Sell':
                    trade = Trade(threshold=threshold,
                        cci_20=self.cci_20[num_candles -1],
                                  cci_40=self.cci_40[num_candles-1],
                                  timestamp=now(),
                                  order_value=2*settings.LOT_SIZE,
                                  price=buy_ticker,
                                  side="Buy")

                    trade.save()
                    self.exchange.cancel_all_orders()

                    buy_orders.append({'price': buy_ticker, 'orderQty':2*settings.LOT_SIZE , 'side': "Buy" })
                self.converge_orders(buy_orders, sell_orders)
                time.sleep(5)
                self.running_qty = self.exchange.get_delta()
                ticker = self.exchange.get_ticker()
                buy_ticker = ticker["sell"]
                sell_ticker =  ticker["buy"]
                while self.running_qty < settings.LOT_SIZE:
                    self.exchange.cancel_all_orders()
                    buy_orders = []
                    sell_orders = []
                    if self.running_qty < settings.LOT_SIZE:
                        buy_orders.append({'price': buy_ticker, 'orderQty': settings.LOT_SIZE -self.running_qty, 'side': "Buy"})
                        print("\nBuy {}\n".format(settings.LOT_SIZE-self.running_qty))

                    elif self.running_qty == 0:
                        buy_orders.append({'price': buy_ticker, 'orderQty': settings.LOT_SIZE, 'side': "Buy"})
                        print("\nBuy {}\n".format(settings.LOT_SIZE))

                    print("\nBuy-EdgeCase:- {} \n".format(self.running_qty))
                    self.converge_orders(buy_orders, sell_orders)
                    time.sleep(5)
                    self.running_qty = self.exchange.get_delta()
                    ticker = self.exchange.get_ticker()
                    buy_ticker = ticker["sell"]
                    sell_ticker =  ticker["buy"]


            elif self.cci_20[num_candles -1]<-threshold and self.cci_40[num_candles-1]<-threshold:
                if last_trade == 'Close':
                    trade = Trade(threshold=threshold,
                        cci_20=self.cci_20[num_candles -1],
                      cci_40=self.cci_40[num_candles-1],
                      timestamp=now(),
                      order_value=settings.LOT_SIZE,
                      price=sell_ticker,
                      side="Sell")
                    trade.save()
                    self.exchange.cancel_all_orders()

                    sell_orders.append({'price': sell_ticker, 'orderQty':settings.LOT_SIZE , 'side': "Sell" })
                elif last_trade == 'Buy':
                    trade = Trade(threshold=threshold,
                        cci_20=self.cci_20[num_candles -1],
                      cci_40=self.cci_40[num_candles-1],
                      timestamp=now(),
                      order_value=2*settings.LOT_SIZE,
                      price=sell_ticker,
                      side="Sell")
                    trade.save()
                    self.exchange.cancel_all_orders()

                    sell_orders.append({'price': sell_ticker, 'orderQty':2*settings.LOT_SIZE , 'side': "Sell" })

                self.converge_orders(buy_orders, sell_orders)
                time.sleep(5)
                self.running_qty = self.exchange.get_delta()
                ticker = self.exchange.get_ticker()
                buy_ticker = ticker["sell"]
                sell_ticker =  ticker["buy"]
                while abs(self.running_qty) < settings.LOT_SIZE:
                    self.exchange.cancel_all_orders()
                    buy_orders = []
                    sell_orders = []
                    if self.running_qty > -settings.LOT_SIZE:

                        sell_orders.append({'price': sell_ticker, 'orderQty': settings.LOT_SIZE + self.running_qty, 'side': "Sell"})
                        print("\Sell {}\n".format(settings.LOT_SIZE+self.running_qty))

                    elif self.running_qty == 0:

                        sell_orders.append({'price': sell_ticker, 'orderQty': settings.LOT_SIZE, 'side': "Sell"})
                        print("\Sell {}\n".format(settings.LOT_SIZE))

                    print("\nSell-EdgeCase:- {} \n".format(self.running_qty))
                    self.converge_orders(buy_orders, sell_orders)

                    time.sleep(5)
                    self.running_qty = self.exchange.get_delta()
                    ticker = self.exchange.get_ticker()
                    buy_ticker = ticker["sell"]
                    sell_ticker =  ticker["buy"]

            else: 
                if last_trade != 'Close':
                    trade = Trade(threshold=threshold,
                        cci_20=self.cci_20[num_candles -1],
                      cci_40=self.cci_40[num_candles-1],
                      timestamp=now(),
                      order_value=settings.LOT_SIZE,
                      price=buy_ticker,
                      side="Close")
                    trade.save()
                    if last_trade == 'Buy':
                        self.exchange.cancel_all_orders()

                        sell_orders.append({'price': sell_ticker, 'orderQty':settings.LOT_SIZE , 'side': "Sell" })
                    else:
                        self.exchange.cancel_all_orders()

                        buy_orders.append({'price': buy_ticker, 'orderQty':settings.LOT_SIZE , 'side': "Buy" })
                    self.converge_orders(buy_orders, sell_orders)
                    time.sleep(5)
                    self.running_qty = self.exchange.get_delta()
                    ticker = self.exchange.get_ticker()
                    buy_ticker = ticker["sell"]
                    sell_ticker =  ticker["buy"]
                while self.running_qty != 0:
                    self.exchange.cancel_all_orders()
                    buy_orders = []
                    sell_orders = []

                    if self.running_qty > 0:
                        sell_orders.append({'price': sell_ticker, 'orderQty': abs(self.running_qty),   'side': "Sell"})
                    elif self.running_qty < 0:
                        sell_orders.append({'price': buy_ticker, 'orderQty':  abs(self.running_qty), 'side': "Buy"})
                    print("\nclose all orders\n")

                    self.converge_orders(buy_orders, sell_orders)
                    time.sleep(5)
                    self.running_qty = self.exchange.get_delta()
                    ticker = self.exchange.get_ticker()
                    buy_ticker = ticker["sell"]
                    sell_ticker =  ticker["buy"]
                    # populate buy and sell orders, e.g.
                    # 
                    # sell_orders.append({'price': 1001.0, 'orderQty': 100, 'side': "Sell"})
        self.tot_mins += 5

        return 

    def run():
        order_manager = CustomOrderManager()

        # Try/except just keeps ctrl-c from printing an ugly stacktrace
        try:
            order_manager.run_loop()
        except (KeyboardInterrupt, SystemExit):
            sys.exit()



def run():
    while True:
        try:
            CustomOrderManager.run()
        except:
            pass
        else:
            break

# def calculate_cci(candles):
#     if self.tot_mins == 0 or (self.tot_mins % 20 == 0 and self.tot_mins % 40 == 0):
#         ar_data =  numpy.column_stack(candles)
#         open_candle = ar_data[1].astype(float)
#         high = ar_data[2].astype(float)
#         low = ar_data[3].astype(float)
#         close = ar_data[4].astype(float)
#         self.cci_20 = talib.CCI(high, low, close, 20)
#         self.cci_40 = talib.CCI(high, low, close, 40)
#     elif self.tot_mins % 20 == 0 and self.tot_mins % 40 != 0:
#         ar_data =  numpy.column_stack(candles)
#         open_candle = ar_data[1].astype(float)
#         high = ar_data[2].astype(float)
#         low = ar_data[3].astype(float)
#         close = ar_data[4].astype(float)
#         self.cci_20 = talib.CCI(high, low, close, 20)
#     elif self.tot_mins % 40 == 0 and self.tot_mins % 20 != 0:
#         ar_data =  numpy.column_stack(candles)
#         open_candle = ar_data[1].astype(float)
#         high = ar_data[2].astype(float)
#         low = ar_data[3].astype(float)
#         close = ar_data[4].astype(float)
#         self.cci_40 = talib.CCI(high, low, close, 40)
#     return self.cci_20, self.cci_40