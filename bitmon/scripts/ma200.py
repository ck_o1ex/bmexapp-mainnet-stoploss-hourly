from __future__ import absolute_import

import os
import time
import talib
import numpy
import ccxt
import sys

from market_maker.market_maker import OrderManager
from market_maker.market_maker import ExchangeInterface
from time import sleep
import sys
from datetime import datetime
from os.path import getmtime
import random
import requests
import atexit
import signal

from market_maker import bitmex
from market_maker.settings import settings
from market_maker.utils import log, constants, errors, math
from bitbot.models import Trade
from tastypie.utils.timezone import now

# Used for reloading the bot - saves modified times of key files


#
# Helpers
#
logger = log.setup_custom_logger('root')
bitmex_ccxt = ccxt.bitmex({"urls":{"api":'https://www.bitmex.com'}})
binance_ccxt = ccxt.binance()

# params:
symbol = 'BTC/USD'
timeframe = '1h'
limit = 750
params = {'partial': False}  # ←--------  no reversal
thresholds = [10,15,20]
bitmex_symbols = {
    'btcusd': 'BTC/USD',
    'ethusd': 'ETH/USD',
    'ethbtc': 'ETHZ18',
    'adabtc': 'ADAZ18',
    'bchbtc': 'BCHZ18',
    'eosbtc': 'EOSZ18',
    'ltcbtc': 'LTCZ18',
    'trxbtc': 'TRXZ18',
    'xrpbtc': 'XRPZ18'}
binance_symbols = {
    'btcusd': 'BTC/USDT',
    'ethusd': 'ETH/USDT',
    'adabtc': 'ADA/BTC',
    'bchbtc': 'BCH/BTC',
    'eosbtc': 'EOS/BTC',
    'ltcbtc': 'LTC/BTC',
    'trxbtc': 'TRX/BTC',
    'xrpbtc': 'XRP/BTC'}


class CustomOrderManager(OrderManager):
    def __init__(self):
        # self.cci_20 = []
        # self.cci_40 = []
        # self.tot_mins = 0
        super(CustomOrderManager, self).__init__()

    """A sample order manager for implementing your own custom strategy"""

    def place_orders(self):
        # implement your custom strategy here
    # pay attention to since with respect to limit if you're doing it in a loop
        buy_orders = [] 
        sell_orders = []
        for key,symbol in binance_symbols.items():
            since = bitmex_ccxt.milliseconds() - limit  *60* 1000 *60
            candles = []
            # ohlcv1 = bitmex_ccxt.fetch_ohlcv(symbol , timeframe, since, limit, params)
            if 'usd' in key:
                candles = binance_ccxt.fetch_ohlcv(symbol , timeframe)
                print(len(candles))
            else:
                candles = binance_ccxt.fetch_ohlcv(symbol , '4h')
                print(len(candles))
                
            num_candles = len(candles)
            ar_data =  numpy.column_stack(candles)
            open_candle = ar_data[1].astype(float)
            high = ar_data[2].astype(float)
            low = ar_data[3].astype(float)
            close = ar_data[4].astype(float)
            ma_200 = talib.MA(close, timeperiod=200, matype=0)
            ma_20 = talib.MA(close, timeperiod=20, matype=0)
            count_ma200 = len(ma_200)
            count_ma20 = len(ma_20)
            lt = Trade.objects.filter(strategy="ma200", symbol=key, exchange='bitmex').last()
            last_trade = lt.side if lt else "Close"
            if symbol == 'BTC/USDT':
                ticker = self.exchange.get_ticker('XBTUSD')
            elif symbol == 'ETH/USDT':
                ticker = self.exchange.get_ticker('ETHUSD')                
            else:
                ticker = self.exchange.get_ticker(bitmex_symbols[key])
            
            buy_ticker = ticker["sell"]
            sell_ticker =  ticker["buy"]
            print(ma_200[count_ma200-1], close[num_candles-1], ma_20[num_candles-1], close[num_candles-1])
            if last_trade !="Close" and lt:
                if last_trade == 'Buy':
                    if close[num_candles-1] < ma_20[num_candles-1]:
                        if low[num_candles-1] < low[num_candles-2]:
                            trade = Trade(timestamp=now(),
                                          order_value=settings.LOT_SIZE,
                                          price=sell_ticker,
                                          side="Close",
                                          strategy="ma200",
                                          exchange="bitmex",
                                          symbol=key)
                            trade.save()
                elif last_trade == "Sell":
                    if close[num_candles-1] > ma_20[num_candles-1]:
                        if high[num_candles-1] > high[num_candles-2]:
                                trade = Trade(timestamp=now(),
                                          order_value=settings.LOT_SIZE,
                                          price=buy_ticker,
                                          side="Close",
                                          strategy="ma200",
                                          exchange="bitmex",
                                          symbol=key)
                                trade.save()

            elif ma_200[count_ma200-1] < close[num_candles-1]:
                if close[num_candles-1] > ma_20[num_candles-1]:
                    if high[num_candles-1] > high[num_candles-2]:
                        if last_trade == 'Close':
                            trade = Trade(timestamp=now(),
                                          order_value=settings.LOT_SIZE,
                                          price=buy_ticker,
                                          side="Buy",
                                          strategy="ma200",
                                          exchange="bitmex",
                                          symbol=key)
                            trade.save()
                        elif last_trade == 'Sell':
                            trade = Trade(timestamp=now(),
                                          order_value=2*settings.LOT_SIZE,
                                          price=buy_ticker,
                                          side="Buy",
                                          strategy="ma200",
                                          exchange="bitmex",
                                          symbol=key)
                            trade.save()
            elif ma_200[count_ma200-1] > close[num_candles-1]:
                if close[num_candles-1] < ma_20[num_candles-1]:
                    if low[num_candles-1] < low[num_candles-2]:
                        if last_trade == 'Close':
                            trade = Trade(timestamp=now(),
                                          order_value=settings.LOT_SIZE,
                                          price=sell_ticker,
                                          side="Sell",
                                          strategy="ma200",
                                          exchange="bitmex",
                                          symbol=key)
                            trade.save()
                        elif last_trade == 'Sell':
                            trade = Trade(timestamp=now(),
                                          order_value=2*settings.LOT_SIZE,
                                          price=sell_ticker,
                                          side="Sell",
                                          strategy="ma200",
                                          exchange="bitmex",
                                          symbol=key)
                            trade.save()
          
        # self.cci_20, self.cci_40 = calculate_cci(candles)
        # if self.tot_mins == 0 or (self.tot_mins % 20 == 15 and self.tot_mins % 40 == 35):
        #     ar_data =  numpy.column_stack(candles)
        #     open_candle = ar_data[1].astype(float)
        #     high = ar_data[2].astype(float)
        #     low = ar_data[3].astype(float)
        #     close = ar_data[4].astype(float)
        #     self.cci_20 = talib.CCI(high, low, close, 20)
        #     self.cci_40 = talib.CCI(high, low, close, 40)
        # elif self.tot_mins % 20 == 15 and self.tot_mins % 40 != 35:
        #     ar_data =  numpy.column_stack(candles)
        #     open_candle = ar_data[1].astype(float)
        #     high = ar_data[2].astype(float)
        #     low = ar_data[3].astype(float)
        #     close = ar_data[4].astype(float)
        #     self.cci_20 = talib.CCI(high, low, close, 20)
        # elif self.tot_mins % 40 == 35 and self.tot_mins % 20 != 15:
        #     ar_data =  numpy.column_stack(candles)
        #     open_candle = ar_data[1].astype(float)
        #     high = ar_data[2].astype(float)
        #     low = ar_data[3].astype(float)
        #     close = ar_data[4].astype(float)
        #     self.cci_40 = talib.CCI(high, low, close, 40)
        # print('\n{}: O: {} H: {} L:{} C:{} cci20:{} cci40:{}\n'.format(
        #     bitmex_ccxt.iso8601(candles[num_candles - 1][0]),
        #     candles[num_candles - 1][1],
        #     candles[num_candles - 1][2],
        #     candles[num_candles - 1][3],
        #     candles[num_candles - 1][4],
        # self.cci_20[num_candles -1], self.cci_40[num_candles-1]))
        # print(len(self.cci_20))

        # margin = self.exchange.get_margin()
        # position = self.exchange.get_position()
        # self.running_qty = self.exchange.get_delta()
        # ticker = self.exchange.get_ticker()
        # buy_ticker = ticker["sell"]
        # sell_ticker =  ticker["buy"]
        # for threshold in thresholds:
        #     lt = Trade.objects.filter(threshold=threshold).last()
        #     last_trade = lt.side if lt else "Close"

        #     if self.cci_20[num_candles -1]>threshold and self.cci_40[num_candles-1]>threshold:
        #         if last_trade == 'Close':
        #             trade = Trade(threshold=threshold,
        #                 cci_20=self.cci_20[num_candles -1],
        #                           cci_40=self.cci_40[num_candles-1],
        #                           timestamp=now(),
        #                           order_value=settings.LOT_SIZE,
        #                           price=buy_ticker,
        #                           side="Buy")
        #             trade.save()
        #         elif last_trade == 'Sell':
        #             trade = Trade(threshold=threshold,
        #                 cci_20=self.cci_20[num_candles -1],
        #                           cci_40=self.cci_40[num_candles-1],
        #                           timestamp=now(),
        #                           order_value=2*settings.LOT_SIZE,
        #                           price=buy_ticker,
        #                           side="Buy")
        #             trade.save()
        #         # self.exchange.cancel_all_orders()

        #         # if self.running_qty < settings.LOT_SIZE:
        #         #     buy_orders.append({'price': buy_ticker, 'orderQty': settings.LOT_SIZE -self.running_qty, 'side': "Buy"})
        #         #     print("\nBuy {}\n".format(settings.LOT_SIZE-self.running_qty))

        #         # elif self.running_qty == 0:
        #         #     buy_orders.append({'price': buy_ticker, 'orderQty': settings.LOT_SIZE, 'side': "Buy"})
        #         #     print("\nBuy {}\n".format(settings.LOT_SIZE))

        #         # print("\nBuy-EdgeCase:- {} \n".format(self.running_qty))


        #     elif self.cci_20[num_candles -1]<-threshold and self.cci_40[num_candles-1]<-threshold:
        #         if last_trade == 'Close':
        #             trade = Trade(threshold=threshold,
        #                 cci_20=self.cci_20[num_candles -1],
        #               cci_40=self.cci_40[num_candles-1],
        #               timestamp=now(),
        #               order_value=settings.LOT_SIZE,
        #               price=sell_ticker,
        #               side="Sell")
        #             trade.save()
        #         elif last_trade == 'Buy':
        #             trade = Trade(threshold=threshold,
        #                 cci_20=self.cci_20[num_candles -1],
        #               cci_40=self.cci_40[num_candles-1],
        #               timestamp=now(),
        #               order_value=2*settings.LOT_SIZE,
        #               price=sell_ticker,
        #               side="Sell")
        #             trade.save()
        #         # self.exchange.cancel_all_orders()

        #         # if self.running_qty > -settings.LOT_SIZE:

        #         #     sell_orders.append({'price': sell_ticker, 'orderQty': settings.LOT_SIZE + self.running_qty, 'side': "Sell"})
        #         #     print("\Sell {}\n".format(settings.LOT_SIZE+self.running_qty))

        #         # elif self.running_qty == 0:

        #         #     sell_orders.append({'price': sell_ticker, 'orderQty': settings.LOT_SIZE, 'side': "Sell"})
        #         #     print("\Sell {}\n".format(settings.LOT_SIZE))

        #         # print("\nSell-EdgeCase:- {} \n".format(self.running_qty))

        #     else: 
        #         if last_trade != 'Close':
        #             trade = Trade(threshold=threshold,
        #                 cci_20=self.cci_20[num_candles -1],
        #               cci_40=self.cci_40[num_candles-1],
        #               timestamp=now(),
        #               order_value=settings.LOT_SIZE,
        #               price=buy_ticker,
        #               side="Close")
        #             trade.save()
        #         # self.exchange.cancel_all_orders()


        #         # if self.running_qty > 0:
        #         #     sell_orders.append({'price': sell_ticker, 'orderQty': abs(self.running_qty),   'side': "Sell"})
        #         # elif self.running_qty < 0:
        #         #     sell_orders.append({'price': buy_ticker, 'orderQty':  abs(self.running_qty), 'side': "Buy"})
        #         # print("\nclose all orders\n")

        #         # self.exchange.cancel_all_orders()
        #         # # populate buy and sell orders, e.g.
        #         # # 
        #         # # sell_orders.append({'price': 1001.0, 'orderQty': 100, 'side': "Sell"})
        return self.converge_orders(buy_orders, sell_orders)
        # self.tot_mins += 5
    def run_loop(self):
        while True:
            sys.stdout.write("-----\n")
            sys.stdout.flush()

            self.check_file_change()
            sleep(1800)

            # This will restart on very short downtime, but if it's longer,
            # the MM will crash entirely as it is unable to connect to the WS on boot.
            if not self.check_connection():
                logger.error("Realtime data connection unexpectedly closed, restarting.")
                self.restart()

            self.sanity_check()  # Ensures health of mm - several cut-out points here
            self.print_status()  # Print skew, delta, etc
            self.place_orders()

    def run():
        order_manager = CustomOrderManager()

        # Try/except just keeps ctrl-c from printing an ugly stacktrace
        try:
            order_manager.run_loop()
        except (KeyboardInterrupt, SystemExit):
            sys.exit()



def run():
    while True:
        try:
            CustomOrderManager.run()
        except:
            pass
        else:
            break
