from __future__ import absolute_import

import os
import time
import talib
import numpy
import ccxt
import sys
import logging

from market_maker.market_maker import OrderManager
from market_maker.market_maker import ExchangeInterface
from time import sleep
import sys
from datetime import datetime, timedelta
from os.path import getmtime
import random
import requests
import atexit
import signal

from market_maker import bitmex
from market_maker.settings import settings
from market_maker.utils import log, constants, errors, math
from bitbot.models import Trade
from tastypie.utils.timezone import now

from google.cloud import logging as gloud_logging
from google.cloud.logging.handlers import CloudLoggingHandler
from google.cloud.logging.handlers import setup_logging
from google.cloud.logging.handlers.handlers import EXCLUDED_LOGGER_DEFAULTS
# Used for reloading the bot - saves modified times of key files
lot_size = {'ADAH19': 500}


# logging = log.setup_custom_logger('stop_ada')
#
# Helpers
# #
bitmex_ccxt = ccxt.bitmex({"urls":{"api":'https://www.bitmex.com'}})

logging_client = gloud_logging.Client()

# The name of the log to write to
log_name = 'macd_ada_hourly_stop'
# Selects the log to write to
handler = CloudLoggingHandler(logging_client, name=log_name)
setup_logging(handler=handler, log_level=logging.INFO, excluded_loggers=EXCLUDED_LOGGER_DEFAULTS)

# params:
symbol = 'ADAH19'
timeframe = '1h'
limit = 750
params = {'partial': False}  # ←--------  no reversal
ccxt_symbol = 'ADAH19'
stop_loss = 0.5

class CustomExchangeInterface(ExchangeInterface):
    def __init__(self, dry_run=False, symbol='ADAH19'):
        self.dry_run = dry_run
        self.symbol = symbol
        self.bitmex = bitmex.BitMEX(base_url="https://www.bitmex.com/api/v1/", symbol=self.symbol,
                                    apiKey=settings.API_KEY, apiSecret=settings.API_SECRET,
                                    orderIDPrefix=settings.ORDERID_PREFIX, postOnly=settings.POST_ONLY,
                                    timeout=settings.TIMEOUT)


class CustomOrderManager(OrderManager):
    def __init__(self):

        self.exchange = CustomExchangeInterface(settings.DRY_RUN,'ADAH19')
        # Once exchange is created, register exit handler that will always cancel orders
        self.symbol = 'ADAH19'
        atexit.register(self.exit)
        signal.signal(signal.SIGTERM, self.exit)

        logging.info("Using symbol %s." % self.exchange.symbol)

        if settings.DRY_RUN:
            logging.info("Initializing dry run. Orders printed below represent what would be posted to BitMEX.")
        else:
            logging.info("Order Manager initializing, connecting to BitMEX. Live run: executing real trades.")

        self.start_time = datetime.now()
        self.instrument = self.exchange.get_instrument()
        self.starting_qty = self.exchange.get_delta()
        self.running_qty = self.starting_qty
        self.reset()

    """A sample order manager for implementing your own custom strategy"""
    def parse_ticker(self):
        ticker = self.exchange.get_ticker(self.symbol)
        sell_ticker = ticker["sell"]
        buy_ticker =  ticker["buy"]
        return buy_ticker, sell_ticker

    def check_running_q(self, action):
        self.running_qty = self.exchange.get_delta(self.symbol)
        if action == 'close':
            condition = self.running_qty !=0
        elif action == 'buy':
            condition = self.running_qty <lot_size[self.symbol]
        elif action == 'sell':
            condition = abs(self.running_qty) <lot_size[self.symbol]

        return condition

    def check_running_p(self, order_price, order_side, action):
        buy_ticker, sell_ticker = self.parse_ticker()
        time.sleep(5)
        if order_side == 'buy':
            while buy_ticker - order_price < 0.001 * order_price and self.check_running_q(action):
                time.sleep(5)
                buy_ticker, sell_ticker = self.parse_ticker()
        elif order_side == 'sell':
            while order_price - sell_ticker < 0.001 * order_price and self.check_running_q(action):
                time.sleep(5)
                buy_ticker, sell_ticker = self.parse_ticker()
        return

    def reset(self):
        self.exchange.cancel_all_orders()
        self.sanity_check()
        self.print_status()
        d = datetime.utcnow()
        diff = ((d.replace(second=0, microsecond=0, minute=0, hour=d.hour) +timedelta(hours=1)) -d).seconds
        # ((d.replace(second=0, microsecond=0, minute=0, hour=d.hour) +timedelta(hours=1)) -d).seconds
        logging.info('{} {}'.format(d, diff))
        # sleep(diff+5)
        # Create orders and converge.
        self.place_orders()

    def process_stop(self, action, switch=1):
        while True:
            self.exchange.cancel_all_orders()
            order_price = self.exchange.get_position()['avgEntryPrice']
            if action == 'buy':

                try:
                    res = self.exchange.bitmex.place_stop_order(order_price - order_price*(stop_loss/100),'Sell')
                except Exception as e:
                    logging.error('converge {}'.format(e))
                    continue
            elif action == 'sell':
                try:
                    res = self.exchange.bitmex.place_stop_order(order_price + order_price*(stop_loss/100),'Buy')
                except Exception as e:
                    logging.error('converge {}'.format(e))
                    continue
            elif action == 'close':
                try:
                    res = self.exchange.cancel_all_orders(stop=True)
                except Exception as e:
                    logging.error('converge {}'.format(e))
                    continue
                else:
                    break
            try:
                if 'Canceled' in res[0]['text']:
                    logging.info('Trade canceled due to volatility')
                    continue
            except KeyError:
                logging.info('stop triggered')
                break
            except:
                logging.error('return error converge')
                continue
            
            break

    def process_order(self, action, switch=1):
        entry = 0
        while self.check_running_q(action):
            if entry > 2:
                sell_ticker, buy_ticker = self.parse_ticker()
                execInst = None
            else:    
                buy_ticker, sell_ticker = self.parse_ticker()
                execInst = 'ParticipateDoNotInitiate'
            self.exchange.cancel_all_orders()
            buy_orders = []
            sell_orders = []
            if action == 'close':

                if self.running_qty > 0:
                    sell_orders.append({'execInst': execInst, 'price': sell_ticker, 'orderQty': abs(self.running_qty), 'side': "Sell"})
                    order_price = sell_ticker
                    order_side = 'sell'
                elif self.running_qty < 0:
                    buy_orders.append({'execInst': execInst, 'price': buy_ticker, 'orderQty':  abs(self.running_qty), 'side': "Buy"})
                    order_price = buy_ticker
                    order_side = 'buy'
                logging.info("close all orders")
            elif action == 'buy':
                    buy_orders.append({'execInst': execInst, 'price': buy_ticker, 'orderQty': switch*lot_size[self.symbol] -self.running_qty, 'side': "Buy"})
                    # 
                    logging.info("Buy {}".format(lot_size[self.symbol]-self.running_qty))
                    order_price = buy_ticker
                    order_side = 'buy'
            elif action == 'sell':
                    sell_orders.append({'execInst': execInst, 'price': sell_ticker, 'orderQty': switch*lot_size[self.symbol] + self.running_qty, 'side': "Sell"})
                    logging.info("Sell {}".format(lot_size[self.symbol]+self.running_qty))
                    order_price = sell_ticker
                    order_side = 'sell'

            logging.info('{} {}'.format(buy_orders, sell_orders))
            try:
                res = self.converge_orders(buy_orders, sell_orders)
            except Exception as e:
                logging.error('converge {}'.format(e))
                continue
            try:
                if 'Canceled' in res[0]['text']:
                    logging.info('Trade canceled due to volatility')
                    continue
            except:
                logging.error('return error converge')
                continue
            self.check_running_p(order_price, order_side, action)
            entry+=1
            self.exchange.cancel_all_orders()
            self.running_qty = self.exchange.get_delta(self.symbol)
        self.process_stop(action, switch)
        return True


    def place_orders(self):
        # implement your custom strategy here
    # pay attention to since with respect to limit if you're doing it in a loop

        since = bitmex_ccxt.milliseconds() - limit  *60* 1000 * 60

        candles = bitmex_ccxt.fetch_ohlcv(ccxt_symbol , timeframe, since, limit, params)
        num_candles = len(candles)

        buy_orders = [] 
        sell_orders = []

        ar_data =  numpy.column_stack(candles)
        # open_candle = ar_data[1].astype(float)
        # high = ar_data[2].astype(float)
        # low = ar_data[3].astype(float)
        close = ar_data[4].astype(float)

        ma_20 = talib.MA(close, timeperiod=20, matype=0)
        ma_50 = talib.MA(close, timeperiod=50, matype=0)
        count_ma20 = len(ma_20)
        count_ma50 = len(ma_50)
        macd, macdsignal, macdhist = talib.MACD(close, fastperiod=12, slowperiod=26, signalperiod=9)
        # macd, macdsignal, macdhist = talib.MACDEXT(close,
        #                                    fastperiod=12,
        #                                    slowperiod=26,
        #                                    signalperiod=9,
        #                                    fastmatype=0,
        #                                    slowmatype=0,
        #                                    signalmatype=1)
        count_macdhist = len(macdhist)
        try:
            lt = Trade.objects.filter(strategy="mamacd", symbol=self.symbol, exchange='bitmex').last()
            last_trade = lt.side
            last_stop= lt.stop
        except:
            last_trade = "Close"
            last_stop = False
        try:
            last_entry = Trade.objects.filter(symbol='ADAH19').order_by('-timestamp')[1]
        except:
            last_entry = Trade(timestamp=now(),
                          side="Close",
                          strategy="mamacd",
                          exchange="bitmex",
                          symbol=self.symbol)
        buy_ticker, sell_ticker = self.parse_ticker()

        if last_trade != 'Close' and lt:

            if last_trade == 'Buy':
                if close[num_candles-1] < ma_20[count_ma20-1] or macdhist[count_macdhist-1] <0:
                    while True:
                        try:
                            resp = self.process_order('close')
                        except Exception as e:
                            logging.error('close buy {} '.format(str(e)))
                            if not resp:
                                continue
                        else:

                            trade = Trade(timestamp=now(),
                                          order_value=lot_size[self.symbol],
                                          price=sell_ticker,
                                          side="Close",
                                          strategy="mamacd",
                                          exchange="bitmex",
                                          symbol=self.symbol)
                            trade.save()
                            break


            elif last_trade == 'Sell':
                if close[num_candles-1] > ma_20[count_ma20-1] or macdhist[count_macdhist-1] >0:
                    while True:
                        try:
                            resp = self.process_order('close')
                        except Exception as e:
                            logging.error('close sell {}'.format(str(e)))
                            if not resp:
                                continue
                        else:
                            trade = Trade(timestamp=now(),
                                          order_value=lot_size[self.symbol],
                                          price=buy_ticker,
                                          side="Close",
                                          strategy="mamacd",
                                          exchange="bitmex",
                                          symbol=self.symbol)
                            trade.save()
                            break


        elif ma_20[count_ma20-1] > ma_50[count_ma50-1]:
            if macdhist[count_macdhist -1] >0 and close[num_candles-1] > ma_20[count_ma20-1]:
                if last_trade == 'Close' and last_stop == False:
                    while True:
                        try:
                            resp = self.process_order('buy')
                        except Exception as e:
                            logging.error('buy close {}'.format(str(e)))
                            if not resp:
                                continue
                        else:
                            trade = Trade(timestamp=now(),
                                          order_value=lot_size[self.symbol],
                                          price=buy_ticker,
                                          side="Buy",
                                          strategy="mamacd",
                                          exchange="bitmex",
                                          symbol=self.symbol)
                            trade.save()
                            break
                elif last_trade == 'Close' and last_stop == True:
                    if last_entry.side =='Buy':
                        if close[num_candles-1] >= float(last_entry.price):
                            logging.info('crossed last entry price')
                            while True:
                                try:
                                    resp = self.process_order('buy')
                                except Exception as e:
                                    logging.error('buy close {}'.format(str(e)))
                                    if not resp:
                                        continue
                                else:
                                    trade = Trade(timestamp=now(),
                                                  order_value=lot_size[self.symbol],
                                                  price=buy_ticker,
                                                  side="Buy",
                                                  strategy="mamacd",
                                                  exchange="bitmex",
                                                  symbol=self.symbol)
                                    trade.save()
                                    break
                        else:
                            logging.info('not crossed last entry price')

                    else:
                        while True:
                            try:
                                resp = self.process_order('buy')
                            except Exception as e:
                                logging.error('buy close {}'.format(str(e)))
                                if not resp:
                                    continue
                            else:
                                trade = Trade(timestamp=now(),
                                              order_value=lot_size[self.symbol],
                                              price=buy_ticker,
                                              side="Buy",
                                              strategy="mamacd",
                                              exchange="bitmex",
                                              symbol=self.symbol)
                                trade.save()
                                break


                elif last_trade == 'Sell':
                    while True:
                        try:
                            resp = self.process_order('buy', 2)
                        except Exception as e:
                            logging.error('buy sell {}'.format(str(e)))
                            if not resp:
                                continue
                        else:
                            trade = Trade(timestamp=now(),
                                          order_value=2*lot_size[self.symbol],
                                          price=buy_ticker,
                                          side="Buy",
                                          strategy="mamacd",
                                          exchange="bitmex",
                                          symbol=self.symbol)
                            trade.save()
                            break

        elif ma_20[count_ma20-1] < ma_50[count_ma50-1]:
            if macdhist[count_macdhist -1] < 0 and close[num_candles-1] < ma_20[count_ma20-1]:
                if last_trade == 'Close' and last_stop == False:
                    while True:
                        try:
                            resp = self.process_order('sell')
                        except Exception as e:
                            logging.error('sell close'.format(str(e)))
                            if not resp:
                                continue
                        else:
                            trade = Trade(timestamp=now(),
                                          order_value=lot_size[self.symbol],
                                          price=sell_ticker,
                                          side="Sell",
                                          strategy="mamacd",
                                          exchange="bitmex",
                                          symbol=self.symbol)
                            trade.save()
                            break
                elif last_trade == 'Close' and last_stop == True:
                    if last_entry.side == 'Sell':
                        if close[num_candles-1] <= float(last_entry.price):
                            logging.info('crossed last entry price')

                            while True:
                                try:
                                    resp = self.process_order('sell')
                                except Exception as e:
                                    logging.error('sell close stop'.format(str(e)))
                                    if not resp:
                                        continue
                                else:
                                    trade = Trade(timestamp=now(),
                                                  order_value=lot_size[self.symbol],
                                                  price=sell_ticker,
                                                  side="Sell",
                                                  strategy="mamacd",
                                                  exchange="bitmex",
                                                  symbol=self.symbol)
                                    trade.save()
                                    break
                        else:
                            logging.info('not crossed last entry price')

                    else:
                        while True:
                            try:
                                resp = self.process_order('sell')
                            except Exception as e:
                                logging.error('sell close stop last entry buy'.format(str(e)))
                                if not resp:
                                    continue
                            else:
                                trade = Trade(timestamp=now(),
                                              order_value=lot_size[self.symbol],
                                              price=sell_ticker,
                                              side="Sell",
                                              strategy="mamacd",
                                              exchange="bitmex",
                                              symbol=self.symbol)
                                trade.save()
                                break


                elif last_trade == 'Buy':
                    while True:
                        try:
                            resp = self.process_order('sell', 2)
                        except Exception as e:
                            logging.error('sell buy'.format(str(e)))
                            if not resp:
                                continue
                        else:
                            trade = Trade(timestamp=now(),
                                          order_value=2*lot_size[self.symbol],
                                          price=sell_ticker,
                                          side="Sell",
                                          strategy="mamacd",
                                          exchange="bitmex",
                                          symbol=self.symbol)
                            trade.save()
                            break

        logging.info('{} {} {} {}'.format(ma_20[count_ma20-1], ma_50[count_ma50-1], macdhist[count_macdhist-1], self.symbol))



    def run_loop(self):
        while True:
            sys.stdout.write("-----\n")
            sys.stdout.flush()

            self.check_file_change()
            d = datetime.utcnow()
            diff = ((d.replace(second=0, microsecond=0, minute=0, hour=d.hour) +timedelta(hours=1)) -d).seconds
            # ((d.replace(second=0, microsecond=0, minute=0, hour=d.hour) +timedelta(hours=1)) -d).seconds
            logging.info('{} {}'.format(d, diff))
            sleep(diff+70)
            stuck = True

            # This will restart on very short downtime, but if it's longer,
            # the MM will crash entirely as it is unable to connect to the WS on boot.
            if not self.check_connection():
                logger.error("Realtime data connection unexpectedly closed, restarting.")
                while stuck:
                    try:
                        self.restart()
                    except Exception as e:
                        if str(e) == "Websocket stuck":
                            logger.info('caught stuck websocket... retrying')
                            continue
                        elif str(e) == "Bitmex Down":
                            logger.info("Bitmex Down caught... retrying")
                            time.sleep(10)
                            continue
                        else:
                            logger.info(str(e))
                            time.sleep(5)
                            continue
                    else:
                        stuck = False

            self.sanity_check()  # Ensures health of mm - several cut-out points here
            self.print_status()  # Print skew, delta, etc
            self.place_orders()

    def run():
        order_manager = CustomOrderManager()

        # Try/except just keeps ctrl-c from printing an ugly stacktrace
        try:
            order_manager.run_loop()
        except (KeyboardInterrupt, SystemExit):
            sys.exit()



def run():
    while True:
        try:

            CustomOrderManager.run()
        except:
            pass
        else:
            break