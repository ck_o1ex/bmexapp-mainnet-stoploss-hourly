from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class BitbotConfig(AppConfig):
    name = 'bitbot'
    verbose_name = _('bitbot')

    def ready(self):
        import bitbot.signals
