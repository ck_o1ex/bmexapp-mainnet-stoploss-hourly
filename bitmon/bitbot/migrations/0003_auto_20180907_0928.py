# Generated by Django 2.0 on 2018-09-07 09:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bitbot', '0002_trade_threshold'),
    ]

    operations = [
        migrations.AddField(
            model_name='trade',
            name='exchange',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='trade',
            name='symbol',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='trade',
            name='cci_20',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='trade',
            name='cci_40',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='trade',
            name='threshold',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
