from django.db import models

# Create your models here.
from tastypie.utils.timezone import now
from django.db import models
from django.utils.text import slugify


class Trade(models.Model):
    cci_20 = models.CharField(max_length=200, null=True, blank=True)
    cci_40 = models.CharField(max_length=200, null=True, blank=True)
    timestamp = models.DateTimeField(default=now)
    order_value = models.CharField(max_length=200)
    price = models.CharField(max_length=200)
    side = models.CharField(max_length=200)
    threshold = models.CharField(max_length=200, null=True, blank=True)
    symbol = models.CharField(max_length=200, null=True, blank=True)
    exchange = models.CharField(max_length=200, null=True, blank=True)
    strategy = models.CharField(max_length=200, null=True, blank=True)
    stop =  models.BooleanField(default=False)

    # # def __unicode__(self):
    # #     return self.title

    # def save(self, *args, **kwargs):
    #     # For automatic slug generation.
    #     if not self.slug:
    #         self.slug = slugify(self.title)[:50]

    #     return super(Entry, self).save(*args, **kwargs)

class TradeProfit(models.Model):
    entry = models.ForeignKey('Trade', blank=True, null=True, related_name='entry', on_delete=models.CASCADE)
    exit = models.ForeignKey('Trade', blank=True, null=True, related_name='exit', on_delete=models.CASCADE)
    pnl = models.CharField(max_length=200, null=True, blank=True)
    status = models.CharField(max_length=200, default='open')