from tastypie.resources import ModelResource
from bitbot.models import Trade, TradeProfit
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

from tastypie import fields, utils

class TradeResource(ModelResource):
    class Meta:
        queryset = Trade.objects.order_by('-id')
        resource_name = 'trade'
        filtering = {
            'threshold': ALL,
            'side': ALL,
            'exchange': ALL,
            'symbol': ALL,
            'strategy':ALL,
            'timestamp': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
        }
    def dehydrate_strategy(self, bundle):
        return bundle.request.GET['custom_strat']


class TradeProfitResource(ModelResource):
    entry = fields.ToOneField(TradeResource, 'entry', full=True, null=True, blank=True)
    exit =  fields.ToOneField(TradeResource, 'exit', full=True, null=True, blank=True)

    class Meta:
        queryset = TradeProfit.objects.order_by('-id')
        resource_name = 'tradeprofit'
        filtering = {
            'profit': ALL,
            'entry': [ALL, ALL_WITH_RELATIONS],
            'exit': [ALL, ALL_WITH_RELATIONS],
            'status': ALL
        }