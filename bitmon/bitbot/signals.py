# from django.db.models.signals import post_save
# from django.dispatch import receiver
# import requests
# from bitbot.models import Trade, TradeProfit

# symbols = ['btcusd','ethusd','ethbtc','adabtc','bchbtc','eosbtc','ltcbtc','trxbtc','xrpbtc']
# lot_size = {'btcusd': 6000, 'ethusd': 6000, 'ethbtc': 30, 'adabtc':90000, 'bchbtc':15, 'eosbtc':1500, 'ltcbtc':150, 'trxbtc':350000, 'xrpbtc':25000}



# @receiver(post_save, sender=Trade)
# def send_trade_signal(sender, instance, **kwargs):
#     if instance.strategy in ['mamacd', 'ma200', 'ma20'] and instance.exchange == 'bitmex':
#         payload={'text': 'strategy: {}\nsymbol: {}\nprice: {}\nside: {}'.format(instance.strategy, instance.symbol,instance.price, instance.side)}
#         res = requests.post('https://hooks.slack.com/services/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', json=payload)
#         print(res)
#         trades = Trade.objects.filter(strategy=instance.strategy, symbol=instance.symbol, exchange=instance.exchange)

#         for i, trade in enumerate(trades):
#             if instance == trade:
#                 if instance.side == 'Close':
#                     prev_trade = trades[i-1]
#                     if prev_trade.side == 'Buy':
#                         if 'usd' in instance.symbol:
#                             pos = lot_size[instance.symbol]/float(prev_trade.price)
#                             profit= (float(instance.price) - float(prev_trade.price))*pos
#                         else:
#                             profit = lot_size[instance.symbol]*float(instance.price) - lot_size[instance.symbol]*float(prev_trade.price)
#                     elif prev_trade.side == 'Sell':
#                         if 'usd' in instance.symbol:
#                             pos = lot_size[instance.symbol]/float(prev_trade.price)
#                             profit = (float(prev_trade.price) - float(instance.price))*pos
#                         else:
#                             profit = lot_size[instance.symbol]*float(prev_trade.price) - lot_size[instance.symbol]*float(instance.price)
#                     try:
#                         tp = TradeProfit.objects.get(entry__id=prev_trade.id)
#                         tp.exit = trade
#                         tp.pnl = profit
#                         tp.status = 'closed'
#                         tp.save()
#                     except Exception as e:
#                         print(instance.id, prev_trade.id, str(e))


#                 else:
#                     tp = TradeProfit(entry=trade, status='open')
#                     tp.save()
#             else:
#                 continue
        
#     elif instance.exchange == 'binance':
#         if instance.side in ['Buy', 'Close']:
#             if float(instance.threshold) > 200:
#                 payload={'text': 'strategy: {}\nsymbol: {}\nprice: {}\nside: {}\nvolume: {}'.format(instance.strategy, instance.symbol,instance.price, instance.side, instance.threshold)}
#                 res = requests.post('https://hooks.slack.com/services/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', json=payload)
#                 print(res)
