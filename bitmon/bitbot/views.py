from django.shortcuts import render
from bitbot.models import Trade
from django.http import JsonResponse



symbols = ['btcusd','ethusd','ethbtc','adabtc','bchbtc','eosbtc','ltcbtc','trxbtc','xrpbtc']
lot_size = {'btcusd': 6000, 'ethusd': 6000, 'ethbtc': 30, 'adabtc':90000, 'bchbtc':15, 'eosbtc':1500, 'ltcbtc':150, 'trxbtc':350000, 'xrpbtc':25000}
# Create your views here.
strategies = ['mamacd', 'ma200', 'ma20']


def pnl(request):
	context = {}
	for strategy in strategies:
		context[strategy] = {}
		for symbol in symbols:
			context[strategy][symbol] = {}
			side = []
			entry = []
			close = []
			t_entry = []
			t_close = []
			trades = Trade.objects.filter(strategy=strategy, symbol=symbol).order_by('timestamp')
			context[strategy][symbol]['trades'] = []
			for trade in trades:
				if trade.side in ['Buy', 'Sell']:
					side.append(trade.side)
					entry.append(trade.price)
					t_entry.append(trade.timestamp)
				else:
					close.append(trade.price)
					t_close.append(trade.timestamp)
			context[strategy][symbol]['trades'].append(list(zip(side,entry,t_entry, close,t_close)))
			context[strategy][symbol]['pnl'] = 0 
			for trades in context[strategy][symbol]['trades']:
				if type(trades) == type('string'):
					continue
				for trade in trades:
					if trade[0] == 'Buy':
						if 'usd' in symbol:
							pos = lot_size[symbol]/float(trade[1])
							context[strategy][symbol]['pnl'] += (float(trade[3]) - float(trade[1]))*pos
						else:
							context[strategy][symbol]['pnl'] += lot_size[symbol]*float(trade[3]) - lot_size[symbol]*float(trade[1])
					elif trade[0] == 'Sell':
						if 'usd' in symbol:
							pos = lot_size[symbol]/float(trade[1])
							context[strategy][symbol]['pnl'] += (float(trade[1]) - float(trade[3]))*pos
						else:
							context[strategy][symbol]['pnl'] += lot_size[symbol]*float(trade[1]) - lot_size[symbol]*float(trade[3])

	return JsonResponse(context)




	# pnl = {}
	# trades_mamacd = Trade.objects.filter(strategy="mamacd").order_by('timestamp')
	# pnl['mamacd'] ={}
	# for symbol in symbols:
	# 	trades = trades_mamacd.filter(symbol=symbol)
	# 	buy_hold = []
	# 	sell_hold = []
	# 	close_hold = []
	# 	for trade in trades:
	# 		if trade.side == 'Buy':
	# 			buy_hold.append(lot_size[symbol]*float(trade.price))
	# 		if trade.side == 'Sell':
	# 			sell_hold.append(lot_size[symbol]*float(trade.price))
	# 		if trade.side == 'Close':
	# 			close_hold.append(lot_size[symbol]*float(trade.price))
	# 	print(buy_hold, sell_hold, close_hold)

	# 	